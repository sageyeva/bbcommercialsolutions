﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ENewsletter.ascx.cs" Inherits="UserControls_ENewsletter" %>


<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>


<asp:UpdatePanel ID="upENewsletter" runat="server">
<ContentTemplate>

    <asp:Panel ID="pnlEnewsletter" runat="server" DefaultButton="btnSubmitEmail">
        <asp:Label ID="lblText" runat="server"></asp:Label>
        
        <br />
        <asp:TextBox ID="txtEmailAddress" runat="server" />
        <asp:Button ID="btnSubmitEmail" runat="server" Text="Enter" 
            onclick="btnSubmitEmail_Click" />
        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
            ControlToValidate="txtEmailAddress" 
            ErrorMessage="Please enter a valid email address." 
            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"> </asp:RegularExpressionValidator>
        <br />
        <strong><asp:Label ID="lblOutput" runat="server"></asp:Label></strong>
    </asp:Panel>

</ContentTemplate>
</asp:UpdatePanel>

<asp:UpdateProgress ID="uENewsletterProgress" runat="server" AssociatedUpdatePanelID="upENewsletter">
<ProgressTemplate>
    <asp:Panel ID="ENewsletterModalWindow" runat="server">
        <div id="gray_bg">
            <div id="formReqModBox">
                <div id="formReqModHead">
                       
                    </div>
                   
                    <br />
                        <img src="../images/loader.gif" alt="loader"/>

                        <h1>Please wait...</h1>
                        <p>Your information is being submitted.</p>
                </div>    
  </div>
</asp:Panel>
</ProgressTemplate>
</asp:UpdateProgress>