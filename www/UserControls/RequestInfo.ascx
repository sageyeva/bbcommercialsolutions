﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RequestInfo.ascx.cs" Inherits="Aviatech.BBCS.View.UserControl_RequestInfo" %>
<%--<asp:ScriptManager ID="ScriptManager1" runat="server">
</asp:ScriptManager>--%>

<%--<link href="http://cdn-na.infragistics.com/igniteui/2016.2/latest/css/themes/infragistics/infragistics.theme.css"
    rel="stylesheet" />
<link href="http://cdn-na.infragistics.com/igniteui/2016.2/latest/css/structure/infragistics.css"
    rel="stylesheet" />--%>

<%--<script src="http://cdn-na.infragistics.com/igniteui/2016.2/latest/js/infragistics.core.js"></script>
<script src="http://cdn-na.infragistics.com/igniteui/2016.2/latest/js/infragistics.lob.js"></script>--%>


<link href="/css/infragistics.theme.css" rel="stylesheet" />
<link href="/css/infragistics.css" rel="stylesheet" />

<script type="text/javascript" src="/js/infragistics.core.js"></script>
<script type="text/javascript" src="/js/infragistics.lob.js"></script>


<script>
    var data = [
            { Name: "Window Coverings" },
            { Name: "Cabinetry" },
            { Name: "Floor Coating" }
     ];

    $(function () {
        $("#cmbProduct").igCombo({
            width: "130px",
            height: "17px",
            dataSource: data,
            textKey: "Name",
            valueKey: "Name",
            multiSelection: {
                enabled: true,
                showCheckboxes: true,
                itemSeparator: ", "
            },
            dropDownClosed: function (event, ui) {
                var prodlist = $("#cmbProduct").igCombo("text");
                document.getElementById("<%= HiddenField1.ClientID %>").value = prodlist;
		    }
        });
    });
</script>

<table cellpadding="0" cellspacing="0" class="float_right">
    <tr>
        <td>
            <label style="color:white; background:#616565;">
                Business:<span class="orange" style="color:white; background:#616565;">*</span></label>
        </td>
        <td>
            <asp:TextBox ID="txtBusinessName" runat="server" Width="130" title="BusinessName"/><br />
            <asp:RequiredFieldValidator ID="rfBusiness" runat="server" ErrorMessage="<span class='required'>required</span>"
                ControlToValidate="txtBusinessName" Display="Dynamic" EnableClientScript="False"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            <label style="color:white; background:#616565;">
                Products:<span class="orange" style="color:white; background:#616565;">*</span></label>
        </td>
        <td>
                <input id="cmbProduct" title="Product"></input>
                         
        </td>
    </tr>
    <tr>
        <td>
            <label style="color:white; background:#616565;">
                Comments:</label>
        </td>
        <td>
            <asp:TextBox ID="txtComment" runat="server" Width="130" title="Comment" />
        </td>
    </tr>
    <tr class="locationsOperated">
        <td>
            <label style="color:white; background:#616565;">
                How many locations
                <br />
                do you operate?</label>
        </td>
        <td>
            <asp:TextBox ID="txtNumOfLoc" runat="server" Width="65px" MaxLength="4" title="NumOfLoc"/><br />
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtNumOfLoc"
                Display="Dynamic" ErrorMessage="<span class='required'>invalid</span>" ValidationExpression="^([0-9]{1,6})?$"
                EnableClientScript="False"></asp:RegularExpressionValidator>
        </td>
    </tr>
</table>

<asp:UpdatePanel ID="upRequestInfo" runat="server">
    <ContentTemplate>
        <asp:Panel ID="pRequestInfo" runat="server">
            <!-- Left Fields -->
            <div class="left_fields">
                <table cellpadding="0" cellspacing="0" class="float_left">
                    <tr>
                        <td>
                            <label style="color:white; background:#616565;">
                                Name:<span class="orange" style="color:white; background:#616565;">*</span></label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtName" runat="server" Width="130" title="Name" /><br />
                            <asp:RequiredFieldValidator ID="rfName" runat="server" ErrorMessage="<span class='required'>required</span>"
                                Display="Dynamic" ControlToValidate="txtName" EnableClientScript="False"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label style="color:white; background:#616565;">
                                Email Address:<span class="orange" style="color:white; background:#616565;">*</span></label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtEmail" runat="server" Width="130" title="email"/><br />
                            <asp:RegularExpressionValidator ID="reEmail" runat="server" ErrorMessage="<span class='required'>invalid</span>"
                                ControlToValidate="txtEmail" Display="Dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                EnableClientScript="False"></asp:RegularExpressionValidator>
                            <asp:RequiredFieldValidator ID="rfEmail" runat="server" ErrorMessage="<span class='required'>required</span>"
                                Display="Dynamic" ControlToValidate="txtEmail" EnableClientScript="False"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label style="color:white; background:#616565;">
                                Phone Number:<span class="orange" style="color:white; background:#616565;">*</span></label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtArea" runat="server" MaxLength="3" Width="30" title="areacode"/>
                            <span class="form-text">-</span>
                            <asp:TextBox ID="txtPhone1" runat="server" MaxLength="3" Width="30" title="phone1"/>
                            <span class="form-text">-</span>
                            <asp:TextBox ID="txtPhone2" runat="server" MaxLength="4" Width="51" title="phone2"/><br />
                            <asp:Literal ID="lPhoneError" Text="<span class='required'>invalid</span>" Visible="true"
                                runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label style="color:white; background:#616565;">
                                Zip/Postal Code:<span class="orange" style="color:white; background:#616565;">*</span></label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtZip" runat="server" Width="130" title="zip"/><br />
                            <asp:RegularExpressionValidator ID="reZip" runat="server" ControlToValidate="txtZip"
                                Display="Dynamic" ErrorMessage="<span class='required'>invalid</span>" ValidationExpression="^(\d{5}-\d{4}|\d{5}|\d{9})$|^([a-zA-Z]\d[a-zA-Z][-\s]?\d[a-zA-Z]\d|[a-zA-Z]\d[a-zA-Z])$"
                                EnableClientScript="False"></asp:RegularExpressionValidator>
                            <asp:RequiredFieldValidator ID="rfPostalCode" runat="server" ErrorMessage="<span class='required'>required</span>"
                                Display="Dynamic" ControlToValidate="txtZip" EnableClientScript="False"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                </table>
            </div>
			<table style="margin-top: -3px; margin-bottom: 2px">
                <tr>
                    <td style="visibility: hidden">
                        <span class="required_fields"><span class="orange" style="color:white; background:#616565;">*</span> Required</span>
                    </td>
                    <td style="visibility: hidden">
                        <span class="required_fields"><span class="form-text">*</span> Required</span>
                    </td>
                    <td style="visibility: hidden">
                        <span class="required_fields"><span class="form-text">*</span> Required</span>
                    </td>
                    <td rowspan="1">
                        <span class="required_fields"><span class="form-text">*</span> Required. To review our <a href="/Privacy-Policy/" alt="Privacy Policy" title="Privacy Policy" target="_blank" ><span class="white" style="text-decoration:underline">Privacy Policy click here</span></a>.
			</span>
                </td>
					<asp:HiddenField ID="HiddenField1" runat="server" />
                    <td>
                        <asp:LinkButton Text="Submit" ID="LinkButton2" runat="server" OnClick="imgSubmit_Click"
                            CssClass="submit">
                              <img src="https://www.bbcommercialsolutions.com/images/submit_btn.png" alt="submit"/>
                        </asp:LinkButton>
                    </td>
                </tr>
            </table>
            
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
<asp:UpdateProgress ID="uProgress" runat="server" AssociatedUpdatePanelID="upRequestInfo">
    <ProgressTemplate>
        <asp:Panel ID="ModalWindow" runat="server">
            <div id="gray_bg">
                <div id="formReqModBox">
                    <div id="formReqModHead">
                    </div>
                    <br />
                    <img src="https://www.bbcommercialsolutions.com/images/loader.gif" alt="loader" />
                    <h1>
                        Please wait...</h1>
                    <p>
                        Your information is being submitted.</p>
                </div>
            </div>
        </asp:Panel>
    </ProgressTemplate>
</asp:UpdateProgress>
