﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ektron.Cms;
using System.Text;
using System.Text.RegularExpressions;

using Aviatech.BBCS.Business;
using Aviatech.BBCS.Model;
using Aviatech.BBCS.Infrastructure.Mail;
using Aviatech.BBCS.Infrastructure;
using System.Configuration;
using System.Collections;
using System.Threading;



using BBCS.BIZ;
using BBCS.BIZ.Data;


namespace Aviatech.BBCS.View
{
    public partial class UserControl_RequestInfo : System.Web.UI.UserControl
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            ClearError();
        }

        private void ClearError()
        {
            this.lPhoneError.Visible = false;
        }

        private void ClearForm()
        {
            this.txtName.Text = string.Empty;
            this.txtEmail.Text = string.Empty;
            this.txtBusinessName.Text = string.Empty;
            this.txtNumOfLoc.Text = string.Empty;
            this.txtArea.Text = string.Empty;
            this.txtPhone1.Text = string.Empty;
            this.txtPhone2.Text = string.Empty;
            this.txtZip.Text = string.Empty;
            this.txtComment.Text = string.Empty;
        }

        private RequestInfoData GetFormData()
        {
            var data = new RequestInfoData()
            {
                Name = this.txtName.Text,
                Phone = this.txtArea.Text + "-" + this.txtPhone1.Text + "-" + this.txtPhone2.Text,
                Email = this.txtEmail.Text,
                PostalCode = this.txtZip.Text,
                BusinessName = this.txtBusinessName.Text,
                Comments = this.txtComment.Text,
                IsSetupAccount = this.txtNumOfLoc.Text,
                Products = this.HiddenField1.Value
            };
            return data;
        }

        private bool IsValidForm(RequestInfoData data)
        {
            return data.Phone.IsValidPhoneNumber();
        }

        private void Save(RequestInfoData data)
        {
            try
            {
                IForm formManager = new RequestInfoForm();
                // formManager.SaveRequestInfo(152, data);
                formManager.SaveRequestInfo(Convert.ToInt64(ConfigurationManager.AppSettings["RequestInfoForm"]), data);
            }
            catch (Exception error)
            {
                string debug = error.Message;
            }
        }

        private void SendFormData(RequestInfoData data)
        {

            try
            {
                string body = new Parser(Server.MapPath(ConfigurationManager.AppSettings["RequestInfoTemplate"].Trim()), GetFormEmailReplacementValues(data)).Parse();

                string from = ConfigurationManager.AppSettings["FromEmailAddress"];
                string to = ConfigurationManager.AppSettings["ToEmailAddress"];
                string cc = ConfigurationManager.AppSettings["CCEmailAddress"];
                string subject = ConfigurationManager.AppSettings["Subject"];
                Helper.SendMail(from, to, cc, subject, body, true);
            }
            catch (Exception error)
            {
                string debug = error.Message;
            }
            finally
            {
            }

        }

        private void SendConfirmationData(RequestInfoData data)
        {

          //  try
           // {
           //     string body = new Parser(Server.MapPath(ConfigurationManager.AppSettings["ConfirmationTemplate"].Trim()), GetConfirmationEmailReplacementValues(data)).Parse();

          //      string from = ConfigurationManager.AppSettings["FromConfirmationEmailAddress"];
           //     string fromName = ConfigurationManager.AppSettings["ConfirmationFromName"];
           //     string to = data.Email;
         //       string cc = string.Empty;
         //       string subject = ConfigurationManager.AppSettings["ConfirmationSubject"];
                // Helper.SendMail(from, to, cc, subject, body, true);
         //       Helper.SendMail(from, fromName, to, string.Empty, cc, subject, body, true);

        //    }
          //  catch (Exception error)
       //     {
         //       string debug = error.Message;
       //     }
      //      finally
     //       {
       //     }

        }

        private void SendToBBCS(RequestInfoData data)
        {
            // Split the name
            var fullName = data.Name;
            string name = "", lastName = "";
            try
            {
                int indexSpace = fullName.IndexOf(" ");
                if (indexSpace != -1)
                {
                    name = fullName.Substring(0, indexSpace).Trim();
                    lastName = fullName.Substring(indexSpace).Trim();
                }
                else
                {
                    name = fullName.Trim();
                }
            }
            catch
            {
                name = fullName;
            }

            // Get FormSource
            var formSource = (Request.ServerVariables["HTTP_REFERER"] != null)
                ? Request.ServerVariables["HTTP_REFERER"].ToString()
                : "";

            // Get LeadSource from cookie
            var leadSource = ReadHFCLeadSourceCookie();

            string brandtype = string.Empty;
           
            if (!string.IsNullOrEmpty(data.Products))
            {
                var products = Regex.Split(data.Products, @",");
                foreach (var product in products)
                {
                    if (product.Trim() == "Floor Coating" || product.Trim() == "Cabinetry") brandtype = brandtype + " TL";
                    if (product.Trim() == "Window Coverings") brandtype = brandtype + " BB";
                }
                
            }

		      var requestForm = new ConsultFormDTO()
            {
                Company = 2,
				CompanyType = 2,
                LocationName = data.BusinessName,
                FirstName = name,
                LastName = lastName,
                Address = "",
                City = "",
                State = "",
                PostalCode = data.PostalCode,
                Email = data.Email,
                Phone = data.Phone,
                RequestUrl = formSource,
                Referrer = "",
                RequestType = "InHome",
                UtmCampaign = leadSource,
                NumOfLoc = data.IsSetupAccount,
                Comment = data.Comments,
				HasMultipleBrand = false,
                BrandType = brandtype
            };
			
			 var result = RequestInHome.Insert(requestForm);
					
		   
        }

        private string ReadHFCLeadSourceCookie()
        {
            string sSourceValue = "Organic";

            if (Request.Cookies != null && Request.Cookies.Count > 0)
            {
                HttpCookie myCookie = Request.Cookies["HFCLeadSource"];
                sSourceValue = (myCookie != null)
                    ? myCookie.Value.ToString()
                    : "Organic";
            }
            return sSourceValue;
        } // end ReadBudgetBlindsCookie()


        private Hashtable GetFormEmailReplacementValues(RequestInfoData data)
        {
            Hashtable result = new Hashtable();
            try
            {

                result.Add("Name", data.Name);
                result.Add("Phone", data.Phone);
                result.Add("Email", data.Email);
                result.Add("Zip", data.PostalCode);
                result.Add("BusinessName", data.BusinessName);
                result.Add("Comment", data.Comments);
                result.Add("NationalAccount", data.IsSetupAccount);

            }
            catch (Exception error)
            {
                string debug = error.Message;
            }
            finally
            {
            }
            return result;
        }

        private Hashtable GetConfirmationEmailReplacementValues(RequestInfoData data)
        {
            Hashtable result = new Hashtable();
            try
            {

                string headerImage = "<img src=\"" + Request.Url.Scheme + "://" + Request.Url.GetComponents(UriComponents.Host, UriFormat.UriEscaped) + "/images/email-template_logo.jpg\"" + " width=\"" + "170\"" + " height=\"" + "212\"" + " border=\"" + "0\"" + " alt=\"" + "Budget Blinds Commercial Solutions\"/>";
                string returnBBCS = "<a href=\"" + Request.Url.Scheme + "://" + Request.Url.GetComponents(UriComponents.Host, UriFormat.UriEscaped) + "/Default.aspx\">" + "<font face=\"arial\"" + " size=\"" + "2\"" + " color=\"" + "#f05033\"" + ">Return to BBCS.</font></a>";
                result.Add("Name", data.Name);
                result.Add("SitePath", headerImage);
                result.Add("ReturnBBC", returnBBCS);
            }
            catch (Exception error)
            {
                string debug = error.Message;
            }
            finally
            {
            }
            return result;
        }


        protected void imgSubmit_Click(object sender, EventArgs e)
        {

            if (Page.IsValid)
            {

                var data = GetFormData();
                if (IsValidForm(data))
                {

                    // Save(data);
                  //  SendFormData(data);
                   // SendConfirmationData(data);
                    // TicketTracker 3596 - Form to submit directly to BBCS database
                    SendToBBCS(data);

                    Response.Redirect(ConfigurationManager.AppSettings["ThankYouPage"], true);
                }
                else
                {

                    this.lPhoneError.Visible = true;
                }
            }

        }
    }
}