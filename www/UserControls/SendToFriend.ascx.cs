﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Configuration;

namespace Aviatech
{
    namespace BudgetBlinds
    {
        public partial class Controls_SendToFriend : System.Web.UI.UserControl
        {
            protected void btnSend_Click(object sender, EventArgs e)
            {
                try
                {
                    formTab.Visible = false;
                    doneTab.Attributes.Remove("style");

                    SaveFormValue();
                    SendMail(txtEMail.Text, txtEMail.Text, ConfigurationManager.AppSettings["sendtofriend_subject_self"]);
                    SendMail(txtEMail.Text, txtRecipientEMail.Text, ConfigurationManager.AppSettings["sendtofriend_subject_friend"]);
                }
                catch (Exception ex)
                {
                    lblErrMsg.Text = ex.Message;
                }
            }

            private void SaveFormValue()
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Ektron.DbConnection"].ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = conn;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandText = "usp_bb_send2friend_Insert";
                    cmd.Parameters.Add(new SqlParameter("@name", txtName.Text));
                    cmd.Parameters.Add(new SqlParameter("@email", txtEMail.Text));
                    cmd.Parameters.Add(new SqlParameter("@recipient_name", txtRecipientName.Text));
                    cmd.Parameters.Add(new SqlParameter("@recipient_email", txtRecipientEMail.Text));
                    cmd.Parameters.Add(new SqlParameter("@message", txtMessage.Text));

                    conn.Open();
                    cmd.ExecuteNonQuery();
                    cmd.Dispose();
                }

            }

            private void SendMail(string from, string to, string subject)
            {
                MailMessage message = new MailMessage();
                message.From = new MailAddress(from);
                message.To.Add(new MailAddress(to));
                message.Subject = subject;
                message.Body = txtMessage.Text;
                message.IsBodyHtml = false;
                SmtpClient client = new SmtpClient();
                client.Host = ConfigurationManager.AppSettings["ek_SMTPServer"].Trim();
                client.Port = Convert.ToInt32(ConfigurationManager.AppSettings["ek_SMTPPort"].Trim());
                client.Send(message);
            }

        }
    }
}