﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SendToFriend.ascx.cs" Inherits="Aviatech.BudgetBlinds.Controls_SendToFriend" %>

<div id="content">

<h1 class="border-bottom">SEND TO A FRIEND</h1>

<div class="padding">
<table id="formTab" runat="server" class="send-to-a-friend">
    <tr>
        <td>
            <table>
                <tr>
                    <td><span>Your Name</span></td>
                    <td><span>Your Email</span></td>
                </tr>
                <tr height="40">
                    <td>
                        <asp:TextBox ID="txtName" runat="server" MaxLength="50" Width="300" Height="25" BackColor="Control"  />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtName" ValidationGroup="formValidation" ErrorMessage="<br />Your Name is required" Display="Dynamic" ForeColor="Red" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtEMail" runat="server" MaxLength="50" Width="300" Height="25" BackColor="Control" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtEMail" ValidationGroup="formValidation" ErrorMessage="<br />Your Email is required" Display="Dynamic" ForeColor="Red" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmail" ValidationGroup="formValidation" ErrorMessage="<br />Your Email Address is invalid" ForeColor="Red" ValidationExpression="^[\w-.]+(?:\+[\w]*)?@([\w-]+.)+[\w-]{2,4}$" Display="Dynamic" />
                    </td>
                </tr>
                <tr>
                    <td><span>Recipient Name</span></td>
                    <td><span>Recipient Email</span></td>
                </tr>
                <tr height="40">
                    <td>
                        <asp:TextBox ID="txtRecipientName" runat="server" MaxLength="50" Width="300" Height="25" BackColor="Control" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtRecipientName" ValidationGroup="formValidation" ErrorMessage="<br />Recipient Name is required" Display="Dynamic" ForeColor="Red" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtRecipientEMail" runat="server" MaxLength="50" Width="300" Height="25" BackColor="Control" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtRecipientEMail" ValidationGroup="formValidation" ErrorMessage="<br />Recipient Email is required" Display="Dynamic" ForeColor="Red" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtRecipientEMail" ValidationGroup="formValidation" ErrorMessage="<br />Recipient Email Address is invalid" ForeColor="Red" ValidationExpression="^[\w-.]+(?:\+[\w]*)?@([\w-]+.)+[\w-]{2,4}$" Display="Dynamic" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><span>Your Message</span></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:TextBox ID="txtMessage" runat="server" MaxLength="255" Rows="4" Columns="80" BackColor="Control" Wrap="true" TextMode="MultiLine" Text="Check out this fantastic franchise opportunity!" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtMessage" ValidationGroup="formValidation" ErrorMessage="<br />Message is required" Display="Dynamic" ForeColor="Red" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><asp:ImageButton ID="btnSend" runat="server" ImageUrl="~/images/send_btn.png" ValidationGroup="formValidation" OnClientClick="OnClickBtnSendHandler()" OnClick="btnSend_Click" /> </td>
                </tr>
           </table>
           <asp:ValidationSummary ID="ValidationSummary" runat="server" ValidationGroup="formValidation" ShowSummary="false" />
           <asp:Label ID="lblErrMsg" runat="server" />
        </td>
    </tr>
</table>
<table id="doneTab" runat="server" style="display:none">
    <tr>
        <td><h2>Thank You</h2>
        <p>Your information has been successfully sent!</p>
        </td>
    </tr>
</table>
<script type="text/javascript">
    function OnClickBtnSendHandler() {
        if (Page_ClientValidate('formValidation')) {
            document.getElementById("<%= formTab.ClientID%>").style.display = 'none'; 
            document.getElementById("<%= doneTab.ClientID%>").style.display = ''; 
        }
    }
</script>
</div>
</div>