﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using hfcService = enewsletter.com.homefranchiseconcepts.websrvcs;

public partial class UserControls_ENewsletter : System.Web.UI.UserControl
{
    #region Property

    public bool IsSubscribe { get; set; }

    public int Concept = 12; //  12=BBCS

    public string ButtonText { get; set; }

    public string LabelText { get; set; }

    #endregion


    #region Global variables

    private string m_sCampaignSource { get; set; }

    private string m_sFormSource { get; set; }

    #endregion


    protected void Page_Init(object sender, EventArgs e)
    {
        lblText.Text = LabelText;
        btnSubmitEmail.Text = ButtonText;
    }

    protected void btnSubmitEmail_Click(object sender, EventArgs e)
    {
        // get the email from viewstage
        string result = "";
        string email = txtEmailAddress.Text;
        txtEmailAddress.Text = "";

        // set up regex to verify that the email is correct
        string sPattern = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
        System.Text.RegularExpressions.Regex checkEmail;
        checkEmail = new System.Text.RegularExpressions.Regex(sPattern);
        
        if (!checkEmail.IsMatch(email))
        {
            lblOutput.Visible = true;
            lblOutput.Text = "Please enter a valid email address.";
            return;
        }

        try
        {
            // Form Source
            m_sFormSource = Request.ServerVariables["HTTP_REFERER"].ToString();

            // Page Source
            m_sCampaignSource = ReadCookie(); // figure out how to get campaign source

            // In the camp mailer database we have a field called UnSub.
            // If Unsub is false then the email address listed is subscribed;
            // If Unsub is true then the email is unsubscribed from all mailing lists.
            // If IsSubscribed is true then subscribe the user by changing the value to false
            // If IsSubscribed is false then change IsSubscribed to true
            bool bIsUnSub = (IsSubscribe) ? false : true;

            // send email to webservice to subscribe or unsubscribe the user
            hfcService.Enewsletter newsletter = new hfcService.Enewsletter();
            result = newsletter.InsertUpdateSubscriptionList(email, Concept, bIsUnSub, m_sCampaignSource, m_sFormSource);
        }
        catch { }

        // show the label and clear the text boxes
        if (IsSubscribe)
            lblOutput.Text = (result.ToLower().Contains("success")) ? "Thank you for subscribing." : result;
        if (!IsSubscribe)
            lblOutput.Text = (result.ToLower().Contains("success")) ? "You have been unsubscribed." : result;

    }

    /// <summary>
    /// Stylesheet.js creates a cookie called "BudgetBlinds". It tracks
    /// the source from referring pages and store them in the cookie.
    /// If ther isn't a referring source, then it's default value is
    /// "Organic".
    /// http://msdn.microsoft.com/en-us/library/aa287533(v=vs.71).aspx
    /// </summary>
    /// <returns></returns>
    private string ReadCookie()
    {
        string sSourceValue = "Organic";
        try
        {
            if (Request.Cookies != null && Request.Cookies.Count > 0)
            {
                HttpCookie myCookie = Request.Cookies["HFCLeadSource"];
                sSourceValue = (myCookie != null)
                    ? myCookie.Value.ToString()
                    : "Organic";
            }
        }
        catch { }
        return sSourceValue;
    } // end ReadBudgetBlindsCookie()
}