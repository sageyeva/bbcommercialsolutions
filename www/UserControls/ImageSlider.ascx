﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ImageSlider.ascx.cs" Inherits="Aviatech.BBCS.View.UserControls_ImageSlider" %>

<%@ Register Assembly="Ektron.Cms.Controls" Namespace="Ektron.Cms.Controls" TagPrefix="CMS" %>


<script type="text/javascript">
    $(function () {

        $('#slider').easySlider({
            auto: true,
            continuous: true,
            numeric: true
        });
    });
</script>

<div>
        
</div>


<asp:MultiView ID="mvViewSet" runat="server" ActiveViewIndex="0">
    <asp:View ID="View" runat="server">
     
             <div id="heroCon">
        <div id="sliderBox">
            <div id="slider">
               <ul>
                <asp:Repeater ID="Repeater1" runat="server">
                    <ItemTemplate>
                         <li style="background:url('<%# DataBinder.Eval (Container.DataItem, "ImageSource") %>') no-repeat;">
					            <!--<div class="slideContent">
						            <p class="slideContentHead" style="font-family:Sans-Serif"><a href="/garage-flooring"><%# DataBinder.Eval (Container.DataItem, "Title") %></a></p>
						            <p class="slideContentText"><a href="/garage-flooring"><%# DataBinder.Eval (Container.DataItem, "Description") %></a></p>
					            </div>-->

					            <a class="slideLink" aria-label="empty" href="<%# DataBinder.Eval (Container.DataItem, "Description") %>"></a>        
				         </li>
                    </ItemTemplate>
                </asp:Repeater>
                </ul>
            </div>
        </div>
     </div>
    
    </asp:View>
    <asp:View ID="Error" runat="server">
        <div>
            <asp:Literal ID="lError" runat="server" />
        </div>
    </asp:View>
</asp:MultiView>