﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

using System.Xml.Linq;

using Aviatech.BBCS.Model;
using Ektron.Cms.Controls;


namespace Aviatech.BBCS.View
{

    public partial class UserControls_ImageSlider : System.Web.UI.UserControl
    {
       ContentBlock _resource;
        protected void Page_Load(object sender, EventArgs e)
        {
            Initialize();
            Display();
        }

        private void Initialize()
        {
           
            ContentBlock data = new ContentBlock()
            {
                 DefaultContentID = Convert.ToInt64(ConfigurationManager.AppSettings["SliderResourceId"]),
                 Page = this.Page
            };
            data.Fill();
            _resource = data;
            
        }

        protected void Display()
        {
            if (this._resource.DefaultContentID > 0)
            {
                var query = Parser(this._resource.EkItem.Html);
                this.Repeater1.DataSource = query;
                this.Repeater1.DataBind();
            }
            else
            {
                this.lError.Text = "Can not find content";
                mvViewSet.SetActiveView(Error);
            }
        }

        private List<ResourceSliderData> Parser(string xml)
        {
            var doc = XDocument.Parse(xml, LoadOptions.None);

            var query = (from item in doc.Descendants("GalleryItem")
                         let image = item.Descendants("img").FirstOrDefault()
                         let id = Convert.ToInt32(item.Element("ID").Value)
                         where item.Element("DisplayImage").Value == "true"
                         select new ResourceSliderData()
                         {
                             Id = id,   
                             Title = item.Element("Title").Value,
                             Description = item.Element("Description").Value,
                             ImageSource = image.Attribute("src").Value,
                             ImageAlt = image.Attribute("alt").Value
                         }).OrderBy(x=>x.Id).ToList();
            if (query != null)
                return query;
            else
                return new List<ResourceSliderData>();

        }
    }
}