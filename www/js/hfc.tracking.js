﻿/********************************************************************************
UPDATE LOG
3/27/09		Added tracking information for the email campaign
4/3/09		Converted financing message to campaign tracking
4/22/09		Added Can PPC TFN
4/22/09		Updated loadme() to check for organic leads before the other variables
5/1/09 		Added 20% CAN-PPC Offer
5/29/09		Commented out form verification code, using vf.js now
*********************************************************************************/

/********************************************************************************
PURPOSE: JavaScript does not contain a String.trim() function that I know of.
This adds my own String.trim() to the String object.
Note: This must be physically located in the page above where it is first used.
*********************************************************************************/
String.prototype.trim = function() { return this.replace(/^\s+|\s+$/g, ''); }

var m_sTimeStamp = '5/1/2009 12:30';
var src = ReadCookie('HFCLeadSource');

/********************************************************************************
PURPOSE: consolidates the old window.onload code with an orphan "if" statement
that I found on the page. This function checks to see if there's a query string.
If so, checks for "src" and "usrid". If it finds them sets cookies "BudgetBlinds"
with "src", and "usrid" with "usrid".
Note: window.onload needs to be called/assigned on the web page, not here. 
Same with window.unload
*********************************************************************************/
LoadMe();

function LoadMe()
{
	//Checks to see if src is present, if not set value to 'Organic'
	if (src == null){
	    SetCookie('HFCLeadSource', "Organic", 30);
	}
	
	if (document.URL.indexOf("?") != -1)
	{
		//utm_campaign functions the same as 'src'
		//We are using this variable so that data is available to Google Analytics on a campaign level
		var source = GetQueryValue("utm_campaign");
		if (source)
		{
		    SetCookie('HFCLeadSource', source, 30);
		}
		
		//if "src" is a querystring, put its value in a cookie called "BudgetBlinds"
		var source = GetQueryValue("src");
		if (source)
		{
		    SetCookie('HFCLeadSource', source, 30);
		}
		
		var sUsrID = GetQueryValue("usrid");
		if (sUsrID)
		{
			SetCookie('usrid', sUsrID, 30);
		}
	}



	var sStyle = ReadCookie("style");
	if (sStyle == null) sStyle = "Green";  
	var oTitle = sStyle ? sStyle : getPreferredStyleSheet();
	
}//end LoadMe()


/********************************************************************************
PURPOSE: Write out campaign variable as a hidden form field
JEB 3/16/09: per Bruce, add a cookie called "usrid", and put its value into 
a hidden field in the form, so it gets POSTed when the form posts.
*********************************************************************************/
function returnSource()
{
    var src = ReadCookie('HFCLeadSource');
	var usrid = ReadCookie('usrid');
	var sHiddenFields = "<input name='src' type='hidden' value='" + src + "' />" +
		"<input name='usrid' type='hidden' value='" + usrid + "' />";	
	document.write(sHiddenFields);
}//end returnSource()

/********************************************************************************
PURPOSE: only the first 2 parameters are required, the cookie name, the cookie
value. Cookie time is in milliseconds, so the below expires will make the 
number you pass in the Set_Cookie function call the number of days the cookie
lasts, if you want it to be hours or minutes, just get rid of 24 and 60.

Generally you don't need to worry about domain, path or secure for most applications
so unless you need that, leave those parameters blank in the function call.

JEB re-factored 3/13/09: per Jerrol, a cookie ought to have an expiration date, 
so if none is given, make the default 30 days.

Note: old name was createCookie()
*********************************************************************************/
function SetCookie(name, value, days, path, domain, secure) 
{
	//if no days are passed, make default 30 days
	var nNumDays = (days) ? days : 30;
	//convert to milliseconds
	nNumDays = nNumDays * 60 * 60 * 24 * 1000;
	//get today's date
	var dtExpires = new Date();
	//convert today's date to milliseconds and add number of days in milliseconds
	dtExpires.setTime(dtExpires.getTime() + nNumDays);
	//assign this mess to the new cookie
	document.cookie = name + "=" + escape(value) + 
	";expires=" + dtExpires.toUTCString() +
	((path)   ? ";path=" + path : ";path=/") + 
	((domain) ? ";domain=" + domain : "") +
	((secure) ? ";secure" : ""); 
	
}//end SetCookie()

/********************************************************************************
PURPOSE: receives name of cookie ("BudgetBlinds", "usrid", or "style" so far)
and returns value of that cookie, if exists, else null.
partly from http://techpatterns.com/downloads/javascript_cookies.php

Note: old name was readCookie()
*********************************************************************************/
function ReadCookie(sSearchName) 
{
	var sCookieValue = null;
	
	//split document.cookie into array of name/value pairs
	var AllCookiesArray = document.cookie.split( ';' );
	
	//iterate array
	for ( i = 0; i < AllCookiesArray.length; i++ )
	{
		//split each name=value pair
		var SingleCookieArray = AllCookiesArray[i].split( '=' );
		
		//see String.prototype.trim above
		var sCookieName = SingleCookieArray[0].trim();
	
		// if the extracted name matches passed sSearchName
		if ( sCookieName == sSearchName )
		{
			//check to see if this cookie actually has a value (2nd half of SingleCookieArray)
			if ( SingleCookieArray.length > 1 )
			{
				//this is the value of the cookie whose name we're searching for
				sCookieValue = unescape(SingleCookieArray[1].trim());
			}			
			break;
		}
	}
	// note that in cases where cookie is initialized but no value, null is returned
	return sCookieValue;
}//end ReadCookie()

/********************************************************************************
PURPOSE: iterates the query string, looking for a particular query string name; 
if it finds it, returns the value
*********************************************************************************/
function GetQueryValue(sName) 
{
	var sValue = "";
	//remove ? from beginning of string
	var query = window.location.search.substring(1);
	var vars = query.split("&");
	for (var i = 0; i < vars.length; i++) 
	{
		var pair = vars[i].split("=");
		if (pair[0] == sName) 
		{
			sValue = pair[1];
		}
	}
	return sValue;
}//end GetQueryValue()

/********************************************************************************
PURPOSE: deletes the given cookie, if found
Note: old name was eraseCookie()
*********************************************************************************/
function DeleteCookie(sCookieName) 
{
	if (ReadCookie(sCookieName))
	{
		SetCookie(sCookieName, "", -1);
	}
}//DeleteCookie()

/******************************** *Begin Test stuff ***********************************************/
function ShowCookies()
{
	var msg = "The cookies associated with this document are: " + document.cookie;
	//document.write(msg);
	alert(msg);
}

function SetThisCookie()
{
	var sName = document.getElementById("txtName").value;
	var sValue = document.getElementById("txtValue").value;
	var nDays = parseInt(document.getElementById("txtDays").value);
	SetCookie(sName, sValue, nDays);
	document.getElementById("txtName").value = '';
	document.getElementById("txtValue").value = '';
	document.getElementById("txtDays").value = "";
	ShowCookies();
}

function FindThisCookie()
{
	var sName = document.getElementById("txtSearchName").value;
	var sValue = ReadCookie(sName);
	document.getElementById("txtSearchValue").value = sValue;
}

function DeleteThisCookie()
{
	var sName = document.getElementById("txtDeleteName").value;
	alert(sName);
	DeleteCookie(sName);
	document.getElementById("txtDeleteName").value = "";
	ShowCookies();
}
/********************************* End Test stuff ***********************************************/
