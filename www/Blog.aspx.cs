﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web.Security;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Ektron.Cms.PageBuilder;
using System.Xml.Linq;



namespace Aviatech.BBCS.View
{

    public partial class Blog :  PageBuilder
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Define the name and type of the client scripts on the page.
            String csname1 = "SocialMediaScript";
            Type cstype = this.GetType();

            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager cs = Page.ClientScript;

            // Check to see if the startup script is already registered.
            if (!cs.IsStartupScriptRegistered(cstype, csname1))
            {
                string cstext1 = "addSocialIcons();";
                cs.RegisterStartupScript(cstype, csname1, cstext1, true);
            }
            //VerifyPhoneNumber();

            Label1.Text = Request.Url.AbsoluteUri;
          
        }

        public override void Error(string message)
        {
            jsAlert(message);
        }

        public override void Notify(string message)
        {
            jsAlert(message);
        }

        public void jsAlert(string message)
        {
            Literal lit = new Literal();
            lit.Text = "<script type=\"\" language=\"\">{0}</script>";
            lit.Text = string.Format(lit.Text, "alert('" + message + "');");
            Form.Controls.Add(lit);
        }

        //private void VerifyPhoneNumber()
        //{
        //    ProcessPhoneNumber();
        //}

        //private string ProcessPhoneNumber()
        //{
        //    var dynamicPhoneConfiguration = Server.MapPath(ConfigurationManager.AppSettings["DynamicPhonePath"]);
        //    var doc = XDocument.Load(dynamicPhoneConfiguration);
        //    string result = string.Empty;
        //    if (Request.QueryString.AllKeys.Contains(ConfigurationManager.AppSettings["DynamicPhoneKey"]))
        //    {
        //        var phoneType = Request.QueryString.Get(ConfigurationManager.AppSettings["DynamicPhoneKey"]);
        //        if (phoneType != null)
        //        {
        //            var phoneNumber = doc.Descendants("Phone")
        //                                    .Where(x => x.Attribute("name").Value.Equals(phoneType))
        //                                    .Select(x => x.Attribute("phonenumber").Value)
        //                                    .First();
        //            if (phoneNumber != null)
        //            {

                       
        //                Session.Add("PhoneNumber", phoneNumber);
        //                ((Literal)this.Master.FindControl("lPhoneTop")).Text = phoneNumber;
        //                ((HtmlGenericControl)this.Master.FindControl("lPhoneBottom")).InnerText = phoneNumber;
        //            }

        //        }

        //    }
        //    else
        //    {
        //        if (Session["PhoneNumber"] != null)
        //        {
        //            var currentPhone = ((string)Session["PhoneNumber"]).ToString();
        //            ((Literal)this.Master.FindControl("lPhoneTop")).Text = currentPhone;
        //            ((HtmlGenericControl)this.Master.FindControl("lPhoneBottom")).InnerText = currentPhone;
        //        }
               
        //    }

        //    return result;
        //}
    }
}