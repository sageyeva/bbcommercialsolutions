<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Blog.aspx.cs" Inherits="Aviatech.BBCS.View.Blog" %>

<%@ Register Assembly="Ektron.Cms.Controls" Namespace="Ektron.Cms.Controls" TagPrefix="CMS" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/PageHost.ascx" TagName="PageHost" TagPrefix="ucPageBuilder" %>
<%@ Register Src="~/Workarea/PageBuilder/PageControls/DropZone.ascx" TagName="DropZone" TagPrefix="ucPageBuilder" %>
<%@ Register Assembly="Ektron.Cms.Widget" Namespace="Ektron.Cms.PageBuilder" TagPrefix="PB" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <ucPageBuilder:PageHost ID="PageHost1" runat="server" />
    <ucPageBuilder:DropZone ID="cmsDropZoneContent" runat="server" AllowColumnResize="true" AllowAddColumn="true">
    </ucPageBuilder:DropZone>


    <style type="text/css">
        .shortURL
        {
            display: none;
        }
    </style>
    

    <script type="text/javascript">
        function addSocialIcons() {
            var href = "";
            var title = "";
            var divs = document.getElementsByTagName("div");
            for (var i = 0; i < divs.length; i++) {
                if (divs[i].className == "entry") {
                    var params = divs[i].getElementsByTagName("p");
                    for (var j = 0; j < params.length; j++) {
                        if (params[j].className == "shortURL") {
                            if (params[j].text != null) {
                                href = params[j].text;
                            }
                            else if (params[j].innerText != null) {
                                href = params[j].innerText;
                            }
                            else {
                                href = params[j].innerHTML;
                            }
                            break;
                        }
                    }
                    if (href == "") {
                        href = window.location.protocol + "//" + window.location.host + getHref(divs[i].childNodes[0].innerHTML);
                    }

                    href = href.replace(/(&nbsp;)/g, '');
                    href = href.trim();
                    //alert(href);
                    //alert(document.getElementById("div_placeholder").innerHTML);

                    if (divs[i].childNodes[0].childNodes[0].text != null) {
                        title = divs[i].childNodes[0].childNodes[0].text;
                    }
                    else {
                        title = divs[i].childNodes[0].childNodes[0].innerText;
                    }
                    title = title.substr(0, 92);
                    //alert(title);

                    var div_placeholder_cloneNode = document.getElementById("div_placeholder").cloneNode(true);

                    var anchors = div_placeholder_cloneNode.getElementsByTagName("a");
                    for (var k=0; k <anchors.length; k++) {
                        anchors[k].href = anchors[k].href.replaceAll("{$url}", href, true);
                         
                        anchors[k].href = anchors[k].href.replaceAll("{$title}", title, true);
                    }

                    var lis = div_placeholder_cloneNode.getElementsByTagName("li");
                    for (var k = 0; k < lis.length; k++) {
                        if (lis[k].className == "ekSocialBarGoogle") {
                            //alert("[" + href + "]");
                            var po = document.createElement('g:plusone');
                            po.setAttribute("href", href);
                            po.setAttribute("size", "small");
                            po.setAttribute("annotation", "none");
                            lis[k].appendChild(po);
                            //alert(lis[k].innerHTML);
                            break;
                        }
                    }
                                      
                    divs[i].innerHTML = div_placeholder_cloneNode.innerHTML + divs[i].innerHTML;
                }
                if (divs[i].className == "blogCommands") {
                    divs[i].innerHTML = divs[i].innerHTML.replaceAll("xid=0", "xid=4294967296");
                }
            }
        }

        function getHref(s) { 
            var pos = s.indexOf("href=");
            s = s.substr(pos + 6);
            pos = s.indexOf('"');
            s = s.substr(0, pos);
            return s;
        }

        String.prototype.replaceAll = function (token, newToken, ignoreCase) {
            var str, i = -1, _token;
            if ((str = this.toString()) && typeof token === "string") {
                _token = ignoreCase === true ? token.toLowerCase() : undefined;
                while ((i = (
            _token !== undefined ?
                str.toLowerCase().indexOf(
                            _token,
                            i >= 0 ? i + newToken.length : 0
                ) : str.indexOf(
                            token,
                            i >= 0 ? i + newToken.length : 0
                )
        )) !== -1) {
                    str = str.substring(0, i)
                    .concat(newToken)
                    .concat(str.substring(i + token.length));
                }
            }
            return str;
        };

        String.prototype.trim = function() {
            return this.replace(/^\s+/, '').replace(/\s+$/, '');
        };
    </script>
  
    <div class="socialBar" style="display:none"> 
        <asp:Label ID="Label1" runat="server" visible="False" />
        <CMS:SocialBar ID="SocialBar1" runat="server" Items="Facebook, Twitter, Email, Google" />
    </div>
  
    <div id="div_placeholder" style="visibility:hidden"> 
        <ul class="ekSocialBar">
            <li class="ekSocialBarFaceBook"> 
                <a target="_blank" title="Share" href="http://www.facebook.com/sharer.php?t=%26s+website&amp;u={$url}"><img alt="Share" title="Share" src="/WorkArea/images/application/bookmarks/FaceBook.png" /> 
                    <span>Facebook</span></a>
	        </li>
            <li class="ekSocialBarTwitter"> 
                <a target="_blank" title="Tweet this..." href="http://twitter.com/home?status={$title} : {$url}"><img alt="Tweet this..." title="Tweet this..." src="/WorkArea/images/application/bookmarks/twitter.png" /> 
                <span>Twitter</span></a>
	        </li>
	        <li class="ekSocialBarEmail"> 
		    <a title="Email This" href="mailto:?subject={$title}&body={$title}: {$url}"><img alt="Email This" title="Email This" src="/WorkArea/images/application/bookmarks/email.gif" /> 
                <span>Email</span></a>
	        </li>
	        <li class="ekSocialBarGoogle"></li><span id="blog-google-plus">Google +</span>
        </ul>
    </div>
    

                    
    <CMS:Blog ID="Blog1" runat="server" BlogID="112" ShowRSS="false" ShowHeader="false" ArchiveMode="Year" />


</asp:Content>

